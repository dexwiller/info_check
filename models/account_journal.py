# -*- coding: utf-8 -*-

from odoo import models, fields, api


class AccountJournal(models.Model):
    _inherit = 'account.journal'
    check_or_bill = fields.Selection([('check', 'Check Journal'), ('bill', 'Bill Journal')], string='Check and Bill Options')
    default_check_journal_in = fields.Boolean(string='Default Check Journal (Incoming)')
    default_check_journal_out = fields.Boolean(string='Default Check Journal (Outgoing)')
    default_bill_journal_in = fields.Boolean(string='Default Bill Journal (Incoming)')
    default_bill_journal_out = fields.Boolean(string='Default Bill Journal (Outgoing)')
    default_endorse_journal = fields.Boolean(string='Default Endorse Journal')

    def update_previous(self, field_name):
        self.search([(field_name, '=', True)]).write({field_name: False})

    def update_previous_prepare(self, values):
        if values.get('default_check_journal_in'):
            self.update_previous('default_check_journal_in')

        if values.get('default_check_journal_out'):
            self.update_previous('default_check_journal_out')

        if values.get('default_bill_journal_in'):
            self.update_previous('default_bill_journal_in')

        if values.get('default_bill_journal_out'):
            self.update_previous('default_bill_journal_out')

        if values.get('default_endorse_journal'):
            self.update_previous('default_endorse_journal')

    @api.model
    def create(self, values):
        self.update_previous_prepare(values)
        return super(AccountJournal, self).create(values)

    def write(self, values):
        self.update_previous_prepare(values)
        return super(AccountJournal, self).write(values)
