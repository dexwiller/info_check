# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions, _, modules
from datetime import datetime as dt
from datetime import date
from pytz import timezone

tz = timezone("Europe/Istanbul")


class Check(models.Model):
    _name = 'info_check.check'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Check'

    @api.model
    def _get_default_check_name(self):
        return self.env['ir.sequence'].sudo().next_by_code('info_check.check_seq')

    @api.model
    def _get_default_amount(self):

        total_payment_amount = self._context.get('payment_amount') or 0.0
        if int(total_payment_amount) == 0:
            return 0
        if not self._context.get('checks'):
            return total_payment_amount
        else:
            top_amount = 0.0
            for c in self._context.get('checks'):
                if not c[2]:
                    top_amount += float(self.browse(c[1]).amount)
                else:
                    top_amount += float(c[2]['amount'])
            return float(total_payment_amount - top_amount)

    @api.model
    def _get_default_journal(self):

        if self._context.get('default_bill'):
            if self._context.get('default_check_type') == 'inbound':
                f_name = 'default_bill_journal_in'
            else:
                f_name = 'default_bill_journal_out'
        else:
            if self._context.get('default_check_type') == 'inbound':
                f_name = 'default_check_journal_in'
            else:
                f_name = 'default_check_journal_out'

        if f_name:
            return self.env.get('account.journal').search([(f_name, '=', True)], order='id desc', limit=1)
        return False

    def _get_journal_domain(self):
        if self._context.get('default_bill'):
            domain = "[('check_or_bill','=','bill'),('company_id', '=', company_id)]"
        else:
            domain = "[('check_or_bill','=','check'),('company_id', '=', company_id)]"

        return domain

    partner_type = fields.Selection([('customer', 'Customer'), ('supplier', 'Vendor')], tracking=True, readonly=True,
                                    states={'draft': [('readonly', False)]})
    partner_id = fields.Many2one('res.partner', string='Partner', tracking=True, readonly=True,
                                 states={'draft': [('readonly', False)]},
                                 domain="['&','|', ('company_id', '=', False), ('company_id', '=', company_id),('is_company','=',True)]")
    journal_id = fields.Many2one('account.journal', string='Journal', required=True, readonly=True,
                                 states={'draft': [('readonly', False)]}, tracking=True,
                                 domain=_get_journal_domain, default=_get_default_journal)
    check_type = fields.Selection([('outbound', 'Given Checks'),
                                   ('inbound', 'Received Checks')],
                                  string='Check Type', required=True, readonly=True,
                                  states={'draft': [('readonly', False)]})
    name = fields.Char(string='Name', default=_get_default_check_name, required=True, readonly=True,
                       states={'draft': [('readonly', False)]})

    state = fields.Selection(string='State', selection=[('draft', 'New'),
                                                        ('port', 'In Portfolio'),
                                                        ('endorsed', 'Endorsed'),
                                                        ('collected', 'Collected'),
                                                        ('in_collateral', 'In Collateral'),
                                                        ('dud', 'Dud'),
                                                        ('returned', 'Returned'),
                                                        ('uncollectible', 'Uncollectible')
                                                        ], default='draft', tracking=True, index=True)
    due_date = fields.Date(string='Due Date', required=True, readonly=True, states={'draft': [('readonly', False)]})
    amount = fields.Float(string='Amount', required=True, default=_get_default_amount, readonly=True,
                          states={'draft': [('readonly', False)]})
    currency = fields.Many2one('res.currency', string='Currency', required=True,
                               default=lambda self: self.env['res.currency'].sudo().search([('name', '=', 'TRY')]).id,
                               readonly=True, states={'draft': [('readonly', False)]})
    check_no = fields.Char(string='Number', required=True, readonly=True, states={'draft': [('readonly', False)]})
    giro = fields.Boolean(string='Giro?', readonly=True, states={'draft': [('readonly', False)]})
    debtor_name = fields.Char(string='Debtors Name', required=True, readonly=True,
                              states={'draft': [('readonly', False)]})

    incoming_payment_id = fields.Many2one('account.payment', string='Incoming Voucher', readonly=True,
                                          states={'draft': [('readonly', False)]})
    outgoing_payment_id = fields.Many2one('account.payment', string='Outgoing Voucher', readonly=True,
                                          states={'draft': [('readonly', False)]})

    payment_city = fields.Char(string='Payment City', required=True, readonly=True,
                               states={'draft': [('readonly', False)]})
    # bank_name           = fields.Many2one('bkm.acquirer', string='Bank Name')
    bank_name = fields.Char(string='Bank Name', required=True, readonly=True, states={'draft': [('readonly', False)]})
    bank_branch = fields.Char(string='Bank Branch', required=True, readonly=True,
                              states={'draft': [('readonly', False)]})
    bank_account_no = fields.Char(string='Bank Account No', required=True, readonly=True,
                                  states={'draft': [('readonly', False)]})
    check_image_fr = fields.Binary(string='Image (Front)', required=True, readonly=True,
                                   states={'draft': [('readonly', False)]})
    check_image_rr = fields.Binary(string='Image (Rear)', required=True, readonly=True,
                                   states={'draft': [('readonly', False)]})

    # t_bank_name            = fields.Many2one('bkm.acquirer', string='Bank Name')
    t_bank_name = fields.Char(string='Bank Name')
    c_partner = fields.Many2one('res.partner', 'Giro Partner')
    t_journal_id = fields.Many2one('account.journal', 'Encash-Deposit Journal')
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id.id, readonly=True,
                                 states={'draft': [('readonly', False)]})

    header_visible = fields.Boolean(compute='get_header_visible')
    check_group_id = fields.Many2one('info_check.check_payroll', string='Payroll')
    expired = fields.Boolean()
    bill = fields.Boolean(string='Is Bill?', default=False)

    @api.constrains('due_date')
    def due_date_control(self):
        now = date.today()
        if self.due_date < now:
            raise exceptions.ValidationError("Due Date Can Not Be Before Then Today.")

    def create_payment(self):
        payment = self.env.get('account.payment')
        payment_methods = self.check_type == 'inbound' and self.journal_id.inbound_payment_method_ids or self.journal_id.outbound_payment_method_ids
        payment_method_id = payment_methods and payment_methods[0] or False
        payment_values = {'journal_id': self.journal_id.id,
                          'amount': self.amount,
                          'partner_type': self.partner_type,
                          'payment_type': self.check_type,
                          'currency_id': self.currency.id,
                          'partner_id': self.partner_id.id,
                          'payment_date': dt.now(),
                          'payment_method_id': payment_method_id.id}

        payment_obj = payment.create(payment_values)
        if payment_obj:
            payment_obj.post()
            return payment_obj.id
        else:
            raise exceptions.ValidationError(_('There is an error in payment creation process'))

    def to_port_action(self, context={}, from_group=False):
        # single port action. if the check is a part of group, so we need to create payment object via group. otherwise this cause a mess in payments.
        # we need to check the group if available and update the group status if necessary

        payment_id = self.create_payment()
        if self.check_type == 'inbound':
            self.incoming_payment_id = payment_id
        elif self.check_type == 'outbound':
            self.outgoing_payment_id = payment_id

        self.state = 'port'

        if self.check_group_id:
            update_group = True
            for c in self.check_group_id.checks:
                if c.state == 'draft':
                    update_group = False

            if update_group and not from_group:
                c.check_group_id.to_port_action()

    def create_reverse_move(self, state):
        # trigger odoo's default reverse move wizard...
        payment_id = self.incoming_payment_id or self.outgoing_payment_id
        if payment_id:
            move_id = payment_id.move_line_ids[0].move_id.id  # we assume that at least one move line bind to check.
            context = {'active_model': 'account.move', 'active_ids': [move_id], 'check_id': self.id, 'state': state}
            action = {
                'name': 'Reverse',
                'type': 'ir.actions.act_window',
                'res_model': 'account.move.reversal',
                'view_mode': 'form',
                'view_id': self.env.ref('account.view_account_move_reversal').id,
                'target': 'new',
                'binding_model_id': self.env.ref('account.model_account_move').id,
                'binding_view_types': 'list',
                'context': context
            }
            return action
        else:
            raise exceptions.ValidationError(_('Can not get the payment. Please check the payments.'))

    def get_header_visible(self):
        self.header_visible = self.company_id.id == self.env.user.company_id.id

    def to_collect_action(self):
        self.write({'state': 'collected',
                    'expired': False})

    def to_return_action(self):
        action = self.create_reverse_move(state='returned')
        return action

    def to_uncollectible_action(self):
        action = self.create_reverse_move(state='uncollectible')
        return action

    def to_dud_action(self):
        action = self.create_reverse_move(state='dud')
        return action

    @api.model
    def cron_update_state(self):
        today = dt.now(tz).date()
        expired_checks = self.sudo().search(
            [('due_date', '<', today),
             ('state', 'in', ('port', 'endorsed', 'in_collateral', 'draft', 'uncollectible'))])
        expired_checks.write({'expired': True})

    @api.model
    def check_user_count(self):
        user_checks = {}
        today = dt.now(tz).date()

        if self.env.user.has_group("info_check.group_check_user"):

            expired_checks = self.search_count([('expired', '=', True), ('bill', '=', False)])
            today_checks = self.search_count(
                [('due_date', '=', today),
                 ('state', 'in', ('port', 'endorsed', 'in_collateral', 'draft', 'uncollectible')),
                 ('bill', '=', False)])

            expired_bills = self.search_count([('expired', '=', True), ('bill', '=', True)])
            today_bills = self.search_count(
                [('due_date', '=', today),
                 ('state', 'in', ('port', 'endorsed', 'in_collateral', 'draft', 'uncollectible')),
                 ('bill', '=', True)])

            if today_checks > 0:
                user_checks['today'] = {
                    'name': _("Today's Checks"),
                    'model': self._name,
                    'icon': modules.module.get_module_icon(
                        self.env[self._name]._original_module),
                    'pending_count': today_checks,
                    'domain': """[('due_date', '=', '{}'),
                               ('state', 'in', ('port', 'endorsed', 'in_collateral', 'draft', 'uncollectible')),
                               ('bill','=',False)]""".format(
                        today),
                    'description': _("Today's Checks")
                }
            if expired_checks > 0:
                user_checks['expired'] = {
                    'name': _("Expired Checks"),
                    'model': self._name,
                    'icon': modules.module.get_module_icon(
                        self.env[self._name]._original_module),
                    'pending_count': expired_checks,
                    'domain': "[('expired', '=', True),('bill','=',False)]",
                    'description': _("Expired Checks")
                }
            if today_bills > 0:
                user_checks['today_bill'] = {
                    'name': _("Today's Bills"),
                    'model': self._name,
                    'icon': modules.module.get_module_icon(
                        self.env[self._name]._original_module),
                    'pending_count': today_bills,
                    'domain': """[('due_date', '=', '{}'),
                               ('state', 'in', ('port', 'endorsed', 'in_collateral', 'draft', 'uncollectible')),
                               ('bill','=',True)]""".format(
                        today),
                    'description': _("Today's Bills")
                }
            if expired_bills > 0:
                user_checks['expired_bills'] = {
                    'name': _("Expired Bills"),
                    'model': self._name,
                    'icon': modules.module.get_module_icon(
                        self.env[self._name]._original_module),
                    'pending_count': expired_bills,
                    'domain': "[('expired', '=', True),('bill','=',True)]",
                    'description': _("Expired Bills")
                }

        return list(user_checks.values())
