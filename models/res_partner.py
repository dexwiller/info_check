# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    checks_in = fields.One2many('info_check.check', 'partner_id', string='Incoming Checks',
                                domain=[('check_type', '=', 'inbound'), ('bill', '=', False)], readonly="1")
    checks_out = fields.One2many('info_check.check', 'partner_id', string='Outgoing Checks',
                                 domain=[('check_type', '=', 'outbound'), ('bill', '=', False)], readonly="1")
    bills_in = fields.One2many('info_check.check', 'partner_id', string='Incoming Bills',
                               domain=[('check_type', '=', 'inbound'), ('bill', '=', True)], readonly="1")
    bills_out = fields.One2many('info_check.check', 'partner_id', string='Outgoing Bills',
                                domain=[('check_type', '=', 'outbound'), ('bill', '=', True)], readonly="1")

    check_payrolls_in = fields.One2many('info_check.check_payroll', 'partner_id', string='Incoming Check Payrolls',
                                        domain=[('payroll_type', '=', 'inbound'), ('bill', '=', False)], readonly="1")
    check_payrolls_out = fields.One2many('info_check.check_payroll', 'partner_id', string='Outgoing Check Payrolls',
                                         domain=[('payroll_type', '=', 'outbound'), ('bill', '=', False)], readonly="1")
    bill_payrolls_in = fields.One2many('info_check.check_payroll', 'partner_id', string='Incoming Bill Payrolls',
                                       domain=[('payroll_type', '=', 'inbound'), ('bill', '=', True)], readonly="1")
    bill_payrolls_out = fields.One2many('info_check.check_payroll', 'partner_id', string='Outgoing Bill Payrolls',
                                        domain=[('payroll_type', '=', 'outbound'), ('bill', '=', True)], readonly="1")
