# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions

state_map = {'port': ['draft'],
             'endorsed': ['port'],
             'collected': ['port', 'endorsed'],
             'in_collateral': ['port']}


class CheckPayroll(models.Model):
    _name = 'info_check.check_payroll'
    _description = 'Check Payroll'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    @api.model
    def _get_default_check_payroll_name(self):
        return self.env['ir.sequence'].sudo().next_by_code('info_check.check_seq')

    @api.model
    def _get_default_journal(self):

        if self._context.get('default_bill'):
            if self._context.get('default_check_type') == 'inbound':
                f_name = 'default_bill_journal_in'
            else:
                f_name = 'default_bill_journal_out'
        else:
            if self._context.get('default_check_type') == 'inbound':
                f_name = 'default_check_journal_in'
            else:
                f_name = 'default_check_journal_out'

        if f_name:
            return self.env.get('account.journal').search([(f_name, '=', True)], order='id desc', limit=1)
        return False

    def _get_journal_domain(self):
        if self._context.get('default_bill'):
            domain = "[('check_or_bill','=','bill'),('company_id', '=', company_id)]"
        else:
            domain = "[('check_or_bill','=','check'),('company_id', '=', company_id)]"

        return domain

    name = fields.Char(string='Payroll Name', default=_get_default_check_payroll_name)
    payroll_type = fields.Selection([('outbound', 'Given Checks'), ('inbound', 'Received Checks')],
                                    string='Payment Type', required=True, readonly=True,
                                    states={'draft': [('readonly', False)]})
    partner_type = fields.Selection([('customer', 'Customer'), ('supplier', 'Vendor')], tracking=True, readonly=True,
                                    states={'draft': [('readonly', False)]})
    partner_id = fields.Many2one('res.partner', string='Partner', tracking=True, readonly=True,
                                 states={'draft': [('readonly', False)]},
                                 domain="['&','|', ('company_id', '=', False), ('company_id', '=', company_id),('is_company','=',True)]")
    amount = fields.Monetary(string='Amount', required=False, readonly=True, states={'draft': [('readonly', False)]},
                             tracking=True, compute="_get_total_amount", store=True)
    currency_id = fields.Many2one('res.currency', string='Currency', required=True, readonly=True,
                                  states={'draft': [('readonly', False)]},
                                  default=lambda self: self.env.company.currency_id)
    journal_id = fields.Many2one('account.journal', string='Journal', required=True, readonly=True,
                                 states={'draft': [('readonly', False)]}, tracking=True,
                                 domain=_get_journal_domain, default=_get_default_journal)
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id.id, readonly=True,
                                 states={'draft': [('readonly', False)]})
    checks = fields.One2many('info_check.check', 'check_group_id', string='Checks', domain=[('bill', '=', False)])
    bills = fields.One2many('info_check.check', 'check_group_id', string='Bills', domain=[('bill', '=', True)])
    amount_by_states = fields.Html(string='Amounts By States', compute="_get_amount_by_states")

    state = fields.Selection(string='State', selection=[('draft', 'New'),
                                                              ('port', 'In Portfolio'),
                                                              ('endorsed', 'Endorsed'),
                                                              ('collected', 'Collected'),
                                                              ('in_collateral', 'In Collateral'),
                                                              ], default='draft', tracking=True)
    header_visible = fields.Boolean(compute='get_header_visible')
    form_editable = fields.Boolean(compute='get_form_editable', default=True)
    bill = fields.Boolean(string='Is Bill?')

    def get_form_editable(self):
        form_editable = True
        if len(self.checks) > 0 or len(self.bills) > 0:
            form_editable = False

        self.form_editable = form_editable

    @api.depends('checks')
    def _get_total_amount(self):
        self.amount = sum([c.amount for c in self.checks or self.bills])

    def _get_amount_by_states(self):
        # maybe simple group by query is the much better way..
        sql = "select state, sum(amount) from info_check_check where check_group_id = {} group by state".format(self.id)
        self.env.cr.execute(sql)
        values = self.env.cr.dictfetchall()
        html = "<div class='table-responsive'><table class='table table-bordered table-hover table-condensed'><tr><th>State</th><th>Amount</th></tr>"
        check_states = self.env.get('info_check.check')._fields.get('state').selection

        for v in values:
            state_text = [s[1] for s in check_states if s[0] == v.get('state')][0]
            html += "<tr><td>{}</td><td>{} {}</td></tr>".format(state_text, v.get('sum'),
                                                                self.currency_id.symbol)
        html += "</table></div>"
        self.amount_by_states = html

    def get_header_visible(self):
        self.header_visible = self.company_id.id == self.env.user.company_id.id

    def get_available_checks_by_state(self, state):
        valid_states = state_map.get(state)
        available_checks = []
        if valid_states:
            children = self.checks or self.bills
            available_checks = children.filtered(
                lambda check: check.state in valid_states and not check.expired)
        return available_checks

    def to_port_action(self):
        draft_checks = self.get_available_checks_by_state('port')
        for c in draft_checks:
            c.to_port_action(from_group=True)
        self.state = 'port'

    @api.model
    def create(self, values):
        checks_or_bills = values.get('checks') or values.get('bills')
        # this necessary because of "create new" option loses the context data.. weird.
        if checks_or_bills:
            for c in checks_or_bills:
                if len(c) == 3 and c[2]:
                    if not c[2].get('journal_id'):
                        c[2]['journal_id'] = values.get('journal_id')
                    if not c[2].get('partner_id'):
                        c[2]['partner_id'] = values.get('partner_id')
                    if not c[2].get('currency'):
                        c[2]['currency'] = values.get('currency_id')
                    if values.get('bill') and not c[2].get('bill'):
                        c[2]['bill'] = values.get('bill')
        res = super(CheckPayroll, self).create(values)
        return res

    def write(self, values):
        checks_or_bills = values.get('checks') or values.get('bills')
        # this necessary because of "create new" option loses the context data.. weird.
        if checks_or_bills:
            for c in checks_or_bills:
                if len(c) == 3 and c[2]:
                    if not c[2].get('journal_id'):
                        c[2]['journal_id'] = self.journal_id.id
                    if not c[2].get('partner_id'):
                        c[2]['partner_id'] = self.partner_id.id
                    if not c[2].get('currency'):
                        c[2]['currency'] = self.currency_id.id
                    if self.bill and not c[2].get('bill'):
                        c[2]['bill'] = self.bill

        res = super(CheckPayroll, self).write(values)
        return res
