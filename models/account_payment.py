# -*- coding: utf-8 -*-

from odoo import models, fields, api, exceptions, _, modules


class AccountPayment(models.Model):
    _inherit = 'account.payment'

    is_check_payment = fields.Boolean(compute='is_check_payment_compute')

    @api.depends('journal_id')
    def is_check_payment_compute(self):
        # print self.journal_id
        if self.journal_id.check:
            self.is_check_payment = True
        else:
            self.is_check_payment = False

    incoming_info_check = fields.One2many('info_check.check', 'incoming_payment_id', string='Incoming Check')
    outgoing_info_check = fields.One2many('info_check.check', 'outgoing_payment_id', string='Outgoing Check')
