# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions
from datetime import datetime as dt


class CollectWizard(models.TransientModel):
    _name = 'info_check.collect_wizard'

    def _default_check(self):
        if self.env.context.get('active_model') == 'info_check.check':
            return self.env['info_check.check'].browse(self._context.get('active_id'))
        return False

    def _default_check_payroll(self):
        if self.env.context.get('active_model') == 'info_check.check_payroll':
            return self.env['info_check.check_payroll'].browse(self._context.get('active_id'))
        return False

    def get_available_checks_default(self):
        if self.env.context.get('active_model') == 'info_check.check_payroll':
            return [(4, c.id) for c in
                    self.env['info_check.check_payroll'].browse(
                        self._context.get('active_id')).get_available_checks_by_state(
                        'collected')]
        return False

    def _default_bank(self):
        if self.env.context.get('active_model') == 'info_check.check_payroll':
            return False
        else:
            return self.env['info_check.check'].browse(self._context.get('active_id')).bank_name

    check_id = fields.Many2one('info_check.check',
                               string="Check", default=_default_check)
    check_group_id = fields.Many2one('info_check.check_payroll', default=_default_check_payroll)

    bank = fields.Char(string='Bank', required=True, default=_default_bank)
    available_payroll_checks = fields.One2many('info_check.check', default=get_available_checks_default,
                                               compute='get_available_checks', string="Available Checks")

    def get_available_checks(self):
        available_checks = False
        if self.check_group_id:
            available_checks = [(4, c.id) for c in self.check_group_id.get_available_checks_by_state('collected')]

        self.available_payroll_checks = available_checks or False

    def save(self):
        if self.check_group_id:
            self.available_payroll_checks.write({'state': 'collected',
                                                 't_bank_name': self.bank})
            self.check_group_id.state = 'collected'
        else:
            self.check_id.write({'state': 'collected',
                                 't_bank_name': self.bank})

        return True


class CollateralWizard(models.TransientModel):
    _name = 'info_check.collateral_wizard'

    # bank    = fields.Many2one('bkm.acquirer', string='Deposit Bank' , required=True)

    def _default_check(self):
        if self.env.context.get('active_model') == 'info_check.check':
            return self.env['info_check.check'].browse(self._context.get('active_id'))
        return False

    def _default_check_payroll(self):
        if self.env.context.get('active_model') == 'info_check.check_payroll':
            return self.env['info_check.check_payroll'].browse(self._context.get('active_id'))
        return False

    def get_available_checks_default(self):
        if self.env.context.get('active_model') == 'info_check.check_payroll':
            return [(4, c.id) for c in
                    self.env['info_check.check_payroll'].browse(
                        self._context.get('active_id')).get_available_checks_by_state(
                        'in_collateral')]
        return False

    def _default_bank(self):
        if self.env.context.get('active_model') == 'info_check.check_payroll':
            return False
        else:
            return self.env['info_check.check'].browse(self._context.get('active_id')).bank_name

    check_id = fields.Many2one('info_check.check',
                               string="Check", required=True, default=_default_check)
    bank = fields.Char(string='Bank', required=True, default=_default_bank)
    check_group_id = fields.Many2one('info_check.check_payroll', default=_default_check_payroll)
    available_payroll_checks = fields.One2many('info_check.check', default=get_available_checks_default,
                                               compute='get_available_checks', string="Available Checks")

    def get_available_checks(self):
        available_checks = False
        if self.check_group_id:
            available_checks = [(4, c.id) for c in self.check_group_id.get_available_checks_by_state('in_collateral')]

        self.available_payroll_checks = available_checks or False

    def save(self):
        if self.check_group_id:
            self.available_payroll_checks.write({'state': 'in_collateral',
                                                 't_bank_name': self.bank})
            self.check_group_id.state = 'in_collateral'
        else:
            self.check_id.write({'state': 'in_collateral',
                                 't_bank_name': self.bank})

        return True


class AccountMoveReversal(models.TransientModel):
    _inherit = 'account.move.reversal'

    def reverse_moves(self):
        res = super(AccountMoveReversal, self).reverse_moves()
        if self.env.context.get('check_id'):
            check = self.env.get('info_check.check').browse([int(self.env.context.get('check_id'))])
            state = self.env.context.get('state')
            if check.state == 'endorsed':
                # we assume that endorsed check has a income move id and reversed first. so we have to reverse the outgoing one recursively.
                # we have to set the state first to break the recursive process.
                check.state = state
                outgoing_move_id = check.outgoing_payment_id.move_line_ids[0].move_id.id
                new_context = self.env.context.copy()
                new_context.update({'active_ids': [outgoing_move_id]})
                self.with_context(new_context).copy({'move_id': outgoing_move_id}).reverse_moves()
            else:
                check.state = state
                if state in ('collected', 'returned', 'dud'):
                    check.expired = False
        else:
            return res


class EndorseWizardSingle(models.TransientModel):
    _name = 'info_check.endorse_wizard'

    def _default_check(self):
        if self.env.context.get('active_model') == 'info_check.check':
            return self.env['info_check.check'].browse(self._context.get('active_id'))
        return False

    def _default_check_payroll(self):
        if self.env.context.get('active_model') == 'info_check.check_payroll':
            return self.env['info_check.check_payroll'].browse(self._context.get('active_id'))
        return False

    def get_available_checks_default(self):
        if self.env.context.get('active_model') == 'info_check.check_payroll':
            return [(4, c.id) for c in
                    self.env['info_check.check_payroll'].browse(
                        self._context.get('active_id')).get_available_checks_by_state(
                        'endorsed') if c.check_type == 'inbound']
        return False

    @api.model
    def _get_default_journal(self):
        return self.env.get('account.journal').search([('default_endorse_journal', '=', True)], order='id desc',
                                                      limit=1)

    def _get_journal_domain(self):

        return "[('company_id', '=', company_id)]"

    check_id = fields.Many2one('info_check.check',
                               string="Check", required=False, default=_default_check)
    partner_id = fields.Many2one('res.partner', string='Endorse to', required=True,
                                 domain="['&','|', ('company_id', '=', False), ('company_id', '=', company_id),('is_company','=',True)]")
    company_id = fields.Many2one('res.company', default=lambda self: self.env.user.company_id.id)
    journal_id = fields.Many2one('account.journal', string='Endorse Journal', required=True,
                                 domain=_get_journal_domain, default=_get_default_journal)

    check_group_id = fields.Many2one('info_check.check_payroll', default=_default_check_payroll)
    available_payroll_checks = fields.One2many('info_check.check', default=get_available_checks_default,
                                               compute='get_available_checks', string="Available Checks")

    def get_available_checks(self):
        available_checks = False
        if self.check_group_id:
            available_checks = [(4, c.id) for c in self.check_group_id.get_available_checks_by_state('endorsed') if
                                c.check_type == 'inbound']

        self.available_payroll_checks = available_checks or False

    def create_payment(self, check_id):
        payment = self.env.get('account.payment')
        payment_methods = self.journal_id.outbound_payment_method_ids
        payment_method_id = payment_methods and payment_methods[0] or False
        payment_values = {'journal_id': self.journal_id.id,
                          'amount': check_id.amount,
                          'partner_type': 'supplier',
                          'payment_type': 'outbound',
                          'currency_id': check_id.currency.id,
                          'partner_id': self.partner_id.id,
                          'payment_date': dt.now(),
                          'payment_method_id': payment_method_id.id}

        payment_obj = payment.create(payment_values)
        if payment_obj:
            payment_obj.post()
            check_id.write({'state': 'endorsed',
                            'outgoing_payment_id': payment_obj.id})
        else:
            raise exceptions.ValidationError(_('There is an error in payment creation process'))

    def save(self):
        if self.check_group_id:
            for c in self.available_payroll_checks:
                self.create_payment(c)
            self.check_group_id.state = 'endorsed'
        else:
            self.create_payment(self.check_id)


class AddFromExisting(models.TransientModel):
    _name = 'info_check.add_from_existing_wizard'

    def _get_check_domain(self):
        if self.env.context.get('payroll_type') == 'inbound':
            domain = [('partner_id', '=', self.env.context.get('partner_id')), ('state', '=', 'port'),
                      ('check_group_id', '=', False), ('check_type', '=', 'inbound'),
                      ('bill', '=', self.env.context.get('bill'))]

        else:
            domain1 = [('check_type', '=', 'inbound'), ('state', '=', 'port'),
                       ('check_group_id', '=', False), ('bill', '=', self.env.context.get('bill'))]
            domain2 = [('check_type', '=', 'outbound'), ('partner_id', '=', self.env.context.get('partner_id')),
                       ('state', '=', 'port'), ('check_group_id', '=', False),
                       ('bill', '=', self.env.context.get('bill'))]
            check_ids = self.env.get('info_check.check').search(domain1).ids + self.env.get('info_check.check').search(
                domain2).ids
            domain = [('id', 'in', check_ids)]

        return domain

    payroll = fields.Many2one('info_check.check_payroll', default=lambda self: self.env.context.get('active_id'))
    partner_id = fields.Many2one('res.partner', default=lambda self: self.env.context.get('partner_id'))
    checks_or_bills = fields.Many2many('info_check.check', domain=_get_check_domain, string='Records to Add',
                                       required=True)

    def save(self):
        record_set = [(4, c.id) for c in self.checks_or_bills]
        if self.payroll.bill:
            self.payroll.bills = record_set
        else:
            self.payroll.checks = record_set