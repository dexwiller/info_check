odoo.define('info_check.systray', function (require) {
    "use strict";

    var config = require('web.config');
    var core = require('web.core');
    var session = require('web.session');
    var SystrayMenu = require('web.SystrayMenu');
    var Widget = require('web.Widget');
    var BusService = require('bus.BusService');

    var QWeb = core.qweb;

    var ExpiredChecksMenu = Widget.extend({
        template:'info_check.CheckReviewMenu',
        events: {
            "show.bs.dropdown": "_onCheckMenuShow",
            'click .o_mail_activity_action': '_onCheckActionClick',
            'click .o_mail_preview': '_onCheckFilterClick',
        },
        start: function () {
            this.$checks_preview = this.$('.o_mail_systray_dropdown_items');
            this._updateCheckPreview();
            var channel = 'info_check.check';
            this.call('bus_service', 'addChannel', channel);
            this.call('bus_service', 'startPolling');
            this.call('bus_service', 'onNotification', this, this._updateCheckPreview);
            return this._super();
        },

        // Private

        _getCheckData: function(){
            var self = this;

            return self._rpc({
                model: 'info_check.check',
                method: 'check_user_count',
                kwargs: {
                    context: session.user_context,
                },
            }).then(function (data) {
                self.checks = data;
                self.checkCounter = _.reduce(data, function(total_count, p_data){ return total_count + p_data.pending_count; }, 0);
                console.log(self.checkCounter);
                self.$('.o_notification_counter').text(self.checkCounter);
                self.$el.toggleClass('o_no_notification', !self.checkCounter);
            });
        },

        _getReviewModelViewID: function (model) {
            return this._rpc({
                model: model,
                method: 'get_activity_view_id'
            });
        },

        _updateCheckPreview: function () {
            var self = this;
            self._getCheckData().then(function (){
                self.$checks_preview.html(QWeb.render('info_check.CheckReviewMenuPreview', {
                    checks : self.checks
                }));
            });
        },

        _updateCounter: function (data) {
            if (data) {
                if (data.check_created) {
                    this.checkCounter ++;
                }
                if (data.check_deleted && this.checkCounter > 0) {
                    this.checkCounter --;
                }
                this.$('.o_notification_counter').text(this.checkCounter);
                this.$el.toggleClass('o_no_notification', !this.checkCounter);
            }
        },

        //------------------------------------------------------------
        // Handlers
        //------------------------------------------------------------

        _onCheckActionClick: function (ev) {
            ev.stopPropagation();
            var actionXmlid = $(ev.currentTarget).data('action_xmlid');
            this.do_action(actionXmlid);
        },

        _onCheckFilterClick: function (event) {
            // fetch the data from the button otherwise fetch the ones from the parent (.o_tier_channel_preview).
            var data = _.extend({}, $(event.currentTarget).data(), $(event.target).data());
            var context = {};
            this.do_action({
                type: 'ir.actions.act_window',
                name: data.model_name,
                res_model:  data.res_model,
                views: [[false, 'list'], [false, 'form']],
                search_view_id: [false],
                domain: data.domain,
                context:context,
            });
        },
        _onCheckMenuShow: function () {
            this._updateCheckPreview();
        },

    });

    SystrayMenu.Items.push(ExpiredChecksMenu);
    return ExpiredChecksMenu;
});
