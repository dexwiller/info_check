# -*- coding: utf-8 -*-
{
    'name': "Checks and Bills",

    'summary': """
        Check Payment Type extension  for Account Voucher Module""",

    'description': """
        Check Payment Type extension  for Account Voucher Module.
        Developed by Deniz Yıldız.
    """,

    'author': "Deniz Yıldız",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'Accounting/Accounting',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account', 'web', 'field_image_preview'],
    'qweb': [
        'static/src/xml/widget.xml',
    ],
    # always loaded
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/account_check_sequence.xml',
        'views/account_payment.xml',
        'views/account_journal.xml',
        'views/account_check.xml',
        'views/account_check_payroll.xml',
        'views/wizards.xml',
        'views/menuitems.xml',
        'views/res_partner.xml',
        'views/assets_backend.xml',
        'data/ir_cron.xml',
    ],
    'qweb': [
        'static/src/xml/systray.xml',
    ],
    'application': True

}
